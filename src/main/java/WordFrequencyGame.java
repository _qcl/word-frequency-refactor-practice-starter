import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";

    public String getResult(String inputStr) {
        try {
            List<WordFrequency> wordFrequencies = splitWordFrequency(inputStr);
            List<WordFrequency> wordFrequenciesWithCount = groupWordFrequencyByCount(wordFrequencies);
            List<WordFrequency> sortedWordFrequencies = sortWordFrequencyByCount(wordFrequenciesWithCount);
            return printWordFrequency(sortedWordFrequencies);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private List<WordFrequency> sortWordFrequencyByCount(List<WordFrequency> list) {
        list.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
        return list;
    }

    private String printWordFrequency(List<WordFrequency> wordFrequencies) {
        return wordFrequencies
                .stream()
                .map(WordFrequency::printWordFrequency)
                .collect(Collectors.joining("\n"));
    }

    private List<WordFrequency> groupWordFrequencyByCount(List<WordFrequency> wordFrequencies) {
        return wordFrequencies
                .stream()
                .collect(Collectors.groupingBy(WordFrequency::getValue))
                .values()
                .stream()
                .map(group -> group.stream()
                        .reduce((w1, w2) -> {
                            w1.setCount(w1.getCount() + w2.getCount());
                            return w1;
                        })
                        .orElse(null))
                .collect(Collectors.toList());
    }

    private List<WordFrequency> splitWordFrequency(String inputStr) {
        String[] words = inputStr.split("\\s+");
        return Arrays.stream(words)
                .map(word -> new WordFrequency(word,1))
                .collect(Collectors.toList());
    }

}
