public class WordFrequency {
    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private int count;

    public WordFrequency(String value, int count){
        this.value = value;
        this.count = count;
    }


    public String getValue() {
        return this.value;
    }

    public int getWordCount() {
        return this.count;
    }

    public String printWordFrequency(){
        return this.getValue() + " " + this.count;
    }

}
